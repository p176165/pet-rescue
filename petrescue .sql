-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2021 at 10:45 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `petrescue`
--

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE `favorite` (
  `id` int(11) NOT NULL,
  `userfrom` int(11) NOT NULL,
  `petid` int(11) NOT NULL,
  `petname` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `favorite`
--

INSERT INTO `favorite` (`id`, `userfrom`, `petid`, `petname`) VALUES
(5, 9, 13, 'Fluffy'),
(7, 25, 17, 'Lucky'),
(9, 9, 25, 'jack233');

-- --------------------------------------------------------

--
-- Table structure for table `pets`
--

CREATE TABLE `pets` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `size` varchar(9) NOT NULL,
  `neutered` varchar(6) NOT NULL,
  `vaccinated` varchar(6) NOT NULL,
  `age` int(5) NOT NULL,
  `bread` varchar(256) NOT NULL,
  `about` varchar(1000) NOT NULL,
  `file` varchar(256) NOT NULL,
  `location` varchar(256) NOT NULL,
  `color` varchar(15) NOT NULL,
  `user` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pets`
--

INSERT INTO `pets` (`id`, `name`, `gender`, `size`, `neutered`, `vaccinated`, `age`, `bread`, `about`, `file`, `location`, `color`, `user`) VALUES
(13, 'Fluffy', 'Female', 'Medium', 'yes', 'yes', 3, 'Poodle', 'Fluffy is a very sweet and active dog, she is ready for a new home!', '104-adoption1.jpg', 'Vilnius', 'white', 'hyhello57@gmail.com'),
(17, 'Lucky', 'Male', 'Medium', 'yes', 'yes', 3, 'Poodle', 'this is  a lucky', '778-adoption6.jpg', 'Kaunas', 'brown', 'hyhello57@gmail.com'),
(23, 'Jack', 'Female', 'Medium', 'yes', 'yes', 2, 'Poodle', 'jack is cute', '364-adoption8.jpg', 'Klaipėda', 'dark brown', 'hyhello57@gmail.com'),
(24, 'Cutesy', 'Male', 'big', 'yes', 'yes', 3, 'Poodle', 'this is another test for moderator', '473-adoption5.jpg', 'Šiauliai', 'white', 'hyhello57@gmail.com'),
(25, 'jack233', 'female', 'small', 'yes', 'yes', 3, 'poodle', 'this is cute and lovely', '125-adoptionsingle2.jpg', 'Vilnius', 'red', 'hyhello57@gmail.com'),
(36, 'mub', 'Female', 'small', 'yes', 'yes', 1, 'Poodle', 'this is a test', '193-adoption8.jpg', 'lahore', 'white', 'hyhello57@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `password` text DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `role` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `password`, `email`, `role`) VALUES
(8, '$2b$10$76xUqKL66pTvvihYH.EwFeIj3Gc23u5AxfJLfOzMlf4drsuhsSZyS', 'hyhello57@gmail.com', 'admin'),
(9, '$2b$10$2OO9ULQbI3R4xkiqMP7G/uAOPCYkfDsqDnC66KDGWQqF0wkzhVKQe', 'admin@test.gmail.com', 'user'),
(22, '$2b$10$muRjBlgppMyOcHTbumXnu.idGQRnmfiHYL6.r5anMF5lx8j4BiVX.', 'hyhello57@gmail.com', 'moderator'),
(25, '$2b$10$O2CzRq57WmfDGTNSFBFboeYsFBC1davPK2RP1fcbsCLeUcUrEFHwK', 'jerrycj11@hotmail.com', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userfrom` (`userfrom`),
  ADD KEY `petid` (`petid`);

--
-- Indexes for table `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pets`
--
ALTER TABLE `pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `favorite`
--
ALTER TABLE `favorite`
  ADD CONSTRAINT `favorite_ibfk_1` FOREIGN KEY (`userfrom`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `favorite_ibfk_2` FOREIGN KEY (`petid`) REFERENCES `pets` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
