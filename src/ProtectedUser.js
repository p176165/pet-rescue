import React from 'react'

const ProtectedUser = () => {
    if(localStorage.getItem("token")){
        return true;
    }
    else{
        return false;
    }
}

export default ProtectedUser
