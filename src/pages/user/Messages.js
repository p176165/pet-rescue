import React ,{useEffect, useState} from 'react';
import '../../App.css';
import { withRouter,Link,Redirect, useParams } from 'react-router-dom';
import io from 'socket.io-client'

let socket;
const CONNECTION_PORT = "localhost:3002/";
socket = io(CONNECTION_PORT,{ transports: ['websocket'] });

const Logout = () => {
   
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    localStorage.removeItem("role");
    localStorage.removeItem("email");
    
    <Redirect to={{ pathname: "/" }}/>
 };



const Messages = () => {

    const { roomid } =useParams()
    //before login
    const [room,setRoom] = useState("hyhello57@gmail.com")
    const [userName,setuserName] = useState("User")

    //afterlogin
    const [message,setMessage] = useState("")
    const [messageList,setMessageList] = useState([])

    useEffect(() => {

        
        socket.emit("join_room" , room)

    })

    useEffect(() => {

        socket.on("receive_message" , (data) => {
            setMessageList([...messageList, data]);
        });

    });

const sendMessage = () => {
    
    let messageContent ={
        room: room,
        content: {
            message: message,
            author: userName
        }
    };

    socket.emit("send_message", messageContent);
    setMessageList([...messageList,messageContent.content]);
    setMessage("");
    

};






    return (
        <div>
            
            <div id="blog-home" class="page">
                <div class="container">
                    <div class="row">

                        <div class="blog-sidebar bg-light-custom  h-50 border-irregular1 col-lg-3 mt-5">

                            <div class="card">
                                <h5 class="card-header">User Menu</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <a href="/user" class="list-group-item list-group-item-action">Favirot List</a>
                                    
                                    </div>
                                </div>
                                <h5 class="card-header">Settings</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <Link to={"/home"} onClick={Logout} class="list-group-item list-group-item-action">Logout</Link>
                                    </div>
                                </div>
                            </div>

                        </div>  

                        <div class="col-lg-9">
                        <div class="container mt-5">
                        <h3 class="card-header">Messaging</h3>
                        <div class="messaging">
                            <div class="inbox_msg">
                                
                                <div class="mesgs">
                                <div class="msg_history">
                                    
                                    
                                    
                                        {messageList.map((value,key) => {
                                            return (
                                                
                                                    <div   id={value.author == userName ? "You" : "Other"}>
                                                        <div class="received_msg">
                                                            <div class="received_withd_msg">
                                                            <p>{value.message}</p>
                                                            <span class="time_date ml-1">{value.author}</span></div>
                                                        </div>
                                                    </div>
                                                

                                            ) 
                                        })}
                                        
                                    
                                </div>
                                <div class="type_msg">
                                    <div class="">
                                    <input type="text"  class="form-control input-field" placeholder="Type a message"   onChange={ (e) =>{ setMessage(e.target.value) } }  />
                                    <button type="submit" id="submit_btn" value="Submit" class="btn btn-primary" onClick={sendMessage} >Send</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                           
                        </div>
                        </div>
                        </div>    
         
                    </div> 
                </div>
            </div>  
        </div>        
    )
}

export default withRouter(Messages)
