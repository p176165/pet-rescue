import React ,{useEffect,useState} from 'react';
import '../../App.css';
import { withRouter,Link,Redirect,useHistory } from 'react-router-dom';
import axios from 'axios';


const Logout = () => {
   
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    localStorage.removeItem("role");
    localStorage.removeItem("email");
    <Redirect to={{ pathname: "/" }}/>
 };


 function fn(text){
    return text.slice(0, 20) + (text.length > 20 ? "..." : "");
}
 
function deletePet(id){
    axios.get(`http://localhost:3002/favorite/deletefavorite/${id}`,{
            headers: {
               "x-access-token" : localStorage.getItem("token")
            },
        }).then((response)=>{
            
            alert("Favorite Remove Successfully!");
            window.location.reload(false);
           
        });
}


const User = () => {

    let i=1;
    const history = useHistory();
    const user = localStorage.getItem("id")
    const [favoritelist, setfavoritelist] = useState([])
    useEffect(() => {
        if(localStorage.getItem("role") !== "user")
        {
            history.push("/login")
        }
        axios.get(`http://localhost:3002/favorite/userfavorites/${user}`,{
            headers: {
               "x-access-token" : localStorage.getItem("token")
            },
        }).then((response)=>{
            if(response.data){
                setfavoritelist(response.data.reverse());
            }
           
        });
   
    },[])


    return (
        <div>
            
        <div id="blog-home" class="page">
            <div class="container">
                <div class="row">

                    <div class="blog-sidebar bg-light-custom  h-50 border-irregular1 col-lg-3 mt-5">

                        <div class="card">
                            <h5 class="card-header">User Menu</h5>
                            <div class="card-body">
                                <div class="list-group">
                                <a href="/User" class="list-group-item list-group-item-action">Favorite List</a>
                                
                                
                                </div>
                            </div>
                            <h5 class="card-header">Settings</h5>
                            <div class="card-body">
                                <div class="list-group">
                                <Link to={"/home"} onClick={Logout} class="list-group-item list-group-item-action">Logout</Link>
                                </div>
                            </div>
                        </div>

                    </div>      
     
                    <div class="col-lg-9">
                        
                    <h3 class="elements-subheader">Favorite List</h3>
                    <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                {favoritelist.map((pet) => {
                                    return  (
                                        <tr key={pet.id}>
                                           <td>{i++}</td>
                                           <td>{pet.petname}</td>
                                           
                                           <td>
                                           <button type="button" class="btn btn-danger "  onClick={() => deletePet(pet.id)}>Delete</button>
                                           </td>
                                        </tr>
                                     )
                                })}
                                

                                </tbody>
                            </table>
                    
                    </div>
    
                    
            </div>
  
         </div>

        </div>
    </div>
    )
}

export default withRouter(User)
