import React, {useState,useEffect} from 'react'
import { Link, useParams, withRouter } from 'react-router-dom';
import axios from 'axios';


const Favorite = (props) => {

    const variables = {
        userfrom: localStorage.getItem("id"),
        petid: props.petid,
        petname: props.petname
    }

    const [myfavorite,setMyFavorite] = useState(false)
    useEffect(() => {
        axios.post("http://localhost:3002/favorite/favoriteAddedcheck",variables,{
            headers: {
               "x-access-token" : localStorage.getItem("token")
            }})
        .then(res => {
            console.log(res)
            if(res.data.check === true)
            {
                
               setMyFavorite(true) 
            }
            
        })
        .catch(err =>{
            alert("Failed to Add into Favorite!")
        })
        
    }, [])

    function addToFavorite () {
        
        
        axios.post("http://localhost:3002/favorite/favoriteAdd",variables,{
            headers: {
               "x-access-token" : localStorage.getItem("token")
            }})
        .then(res => {
            setMyFavorite(true)
        })
        .catch(err =>{
            alert("Failed to Add into Favorite!")
        })
    }



    return (
        <div>
            <a href="#" class="btn btn-primary " data-aos="zoom-in"onClick={() => addToFavorite()}  >{myfavorite ? "Not Favorite" : "Add to Favorite"}</a>
        </div>
    )
}

export default withRouter(Favorite)
