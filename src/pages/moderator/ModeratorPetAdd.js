import  { useState,useEffect } from 'react';
import { useHistory,Link, Redirect, withRouter } from 'react-router-dom';
import axios from 'axios';


const Logout = () => {
   
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    localStorage.removeItem("role");
    localStorage.removeItem("email");
    <Redirect to={{ pathname: "/" }}/>
 };
 
const ModeratorPetAdd = () => {

    const history = useHistory();
    useEffect(() => {
        if(localStorage.getItem("role") !== "moderator")
        {
            history.push("/login")
        }
        
   
    })

    const [name,setName] = useState("")
    const [gender,setGender] = useState("")
    const [size,setSize] = useState("")
    const [neutered,setNeutered] = useState("")
    const [vaccinated,setVaccinated] = useState("")
    const [age,setAge] = useState("")
    const [bread,setBread] = useState("")
    const [description,setAbout] = useState("")
    const [location,setLocation] = useState("")
    const [color,setColor] = useState("")
    
    const [file,setFile] = useState("")
    const [petStatus,setpetStatus] = useState("")

    const send = async (event) => {
        const email = localStorage.getItem("email")
        
        const data = new FormData()
        data.append("name",name);
        data.append("gender",gender);
        data.append("size",size);
        data.append("neutered",neutered);
        data.append("vaccinated",vaccinated);
        data.append("age",age);
        data.append("bread",bread);
        data.append("about",description);
        data.append("email",email);
        data.append("location",location);
        data.append("color",color);
        data.append("file",file);
        

        

      await  axios.post("http://localhost:3002/pet/addPet",data)
        .then(res => {
            setpetStatus(res.data.data)
        })
        .catch(err =>{
            setpetStatus(err);
        })
    };   



    return (
        <div>
            
            <div id="blog-home" class="page">
                <div class="container">
                    <div class="row">

                        <div class="blog-sidebar bg-light-custom  h-50 border-irregular1 col-lg-3 mt-5">

                            <div class="card">
                                <h5 class="card-header">Moderator Menu</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <a href="/moderator" class="list-group-item list-group-item-action">Pets List</a>
                                    <a href="/ModeratorPetAdd" class="list-group-item list-group-item-action">Add New Pet</a>
                                    <a href="/ModeratorMessage" class="list-group-item list-group-item-action">Messages</a>
                                    
                                    </div>
                                </div>
                                <h5 class="card-header">Settings</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <Link to={"/home"} onClick={Logout} class="list-group-item list-group-item-action">Logout</Link>
                                    </div>
                                </div>
                            </div>

                        </div>      
         
                        <div class="col-lg-9">
                            <h3 class="elements-subheader">Add New Pet</h3>
                            
                            <h6>{petStatus}</h6>
                            <div id="contact_form">
                                <div class="form-group">
                                    <form>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Name<span class="required">*</span></label>
                                            <input type="text" name="name" class="form-control input-field" required="" placeholder="Fluffy" onChange={ (e) =>{ setName(e.target.value) } }  />

                                        </div>
                                        <div class="col-md-4">
                                            <label>Gender<span class="required">* : Male,Female</span></label>
                                            <input type="text" name="gender" class="form-control input-field" required="" placeholder="Female" onChange={ (e) =>{ setGender(e.target.value) } }  />

                                        </div>
                                        <div class="col-md-4">
                                            <label>Size<span class="required">* : Small,Medium,Big</span></label>
                                            <input type="text" name="size" class="form-control input-field" required="" placeholder="Medium" onChange={ (e) =>{ setSize(e.target.value) } }   />

                                        </div>
                                        
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                        <label>Neutered<span class="required">*: Yes,No</span></label>
                                            <input type="text" name="neutered" class="form-control input-field"  required="" placeholder="Yes" onChange={ (e) =>{ setNeutered(e.target.value) } }   />

                                        </div>
                                        <div class="col-md-3">
                                            <label>Vaccinated<span class="required">*: Yes,No</span></label>
                                            <input type="text" name="vaccinated" class="form-control input-field"  required="" placeholder="Yes" onChange={ (e) =>{ setVaccinated(e.target.value) } }  />
                                            
                                        </div>
                                        <div class="col-md-2">
                                            <label>Age<span class="required">*</span></label>
                                            <input type="number" name="age" class="form-control input-field"  required="" placeholder="2" onChange={ (e) =>{ setAge(e.target.value) } }  />
                                            
                                        </div>
                                        <div class="col-md-4">
                                            <label>Breed<span class="required">*</span></label>
                                            <input type="text" name="bread" class="form-control input-field" placeholder="Poodle" onChange={ (e) =>{ setBread(e.target.value) } } />
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <label>Location<span class="required">*</span></label>
                                            <input type="text" name="location" class="form-control input-field"  onChange={ (e) =>{ setLocation(e.target.value) } }  />
                                        </div>
                                        <div class="col-md-4">
                                            <label>Color<span class="required">*</span></label>
                                            <input type="text" name="color" class="form-control input-field"  onChange={ (e) =>{ setColor(e.target.value) } }  />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Image</label>
                                            <input type="file" name="file" class="form-control input-field" accept="image/*" onChange={ (e) =>{ setFile(e.target.files[0]) } }  />
                                        </div>
                                        <div class="col-md-12">
                                            <label>About<span class="required">*</span></label>
                                            <textarea name="about"  class="textarea-field form-control" rows="3" required=""  onChange={ (e) =>{ setAbout(e.target.value) } }  />
                                        </div>
                                    </div>
                                    <button type="submit" id="submit_btn" value="Submit" class="btn btn-primary" onClick={send} >Add Now</button>
                                    </form>
                                </div>

                                <div id="contact_results"></div>
                            </div>
                            
        
                        
                </div>
      
             </div>
   
            </div>
            
        </div>
        </div>
    )
}

export default withRouter( ModeratorPetAdd ) 
