import React ,{useEffect,useState} from 'react';
import '../../App.css';
import { withRouter,Link,Redirect,useHistory } from 'react-router-dom';
import axios from 'axios';

const Logout = () => {
   
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    localStorage.removeItem("role");
    localStorage.removeItem("email");
    
    <Redirect to={{ pathname: "/" }}/>
 };

function fn(text){
    return text.slice(0, 20) + (text.length > 20 ? "..." : "");
}
 
function deletePet(id){
    axios.get(`http://localhost:3002/pet/deletepet/${id}`,{
            headers: {
               "x-access-token" : localStorage.getItem("token")
            },
        }).then((response)=>{
            
            alert("Pet Deleted Successfully!");
            window.location.reload(false);
           
        });
}

const Moderator = () => {

    const history = useHistory();
    const user = localStorage.getItem("email")
    const [petlist, setpetlist] = useState([])
    useEffect(() => {
        if(localStorage.getItem("role") !== "moderator")
        {
            history.push("/login")
        }
        axios.get(`http://localhost:3002/pet/Modpets/${user}`,{
            headers: {
               "x-access-token" : localStorage.getItem("token")
            },
        }).then((response)=>{
            if(response.data){
                setpetlist(response.data.reverse());
            }
           
        });
   
    },[])


    return (
        <div>
            
            <div id="blog-home" class="page">
                <div class="container">
                    <div class="row">

                        <div class="blog-sidebar bg-light-custom  h-50 border-irregular1 col-lg-3 mt-5">

                            <div class="card">
                                <h5 class="card-header">Moderator Menu</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <a href="/moderator" class="list-group-item list-group-item-action">Pets List</a>
                                    <a href="/ModeratorPetAdd" class="list-group-item list-group-item-action">Add New Pet</a>
                                    <a href="/ModeratorMessage" class="list-group-item list-group-item-action">Messages</a>
                                    
                                    </div>
                                </div>
                                <h5 class="card-header">Settings</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <Link to={"/home"} onClick={Logout} class="list-group-item list-group-item-action">Logout</Link>
                                    </div>
                                </div>
                            </div>

                        </div>      
         
                        <div class="col-lg-9">
                            
                        <h3 class="elements-subheader">Pets List</h3>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Size</th>
                                        <th>Gender</th>
                                        <th>Age</th>
                                        <th>Neutered</th>
                                        <th>About</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                {petlist.map((pet) => {
                                    return  (
                                        <tr key={pet.id}>
                                           <td>{pet.name}</td>
                                           <td>{pet.size}</td>
                                           <td>{pet.gender}</td>
                                           <td>{pet.age}</td>
                                           <td>{pet.neutered}</td>
                                           <td>{fn(pet.about)}</td>
                                           
                                           <td>
                                           <Link to={`/ModUpDatePet/${pet.id}`}><button type="button" class="btn btn-primary " >Update</button></Link>   
                                           </td>
                                           <td>
                                           <button type="button" class="btn btn-danger "  onClick={() => deletePet(pet.id)}>Delete</button>
                                           </td>
                                        </tr>
                                     )
                                })}
                                

                                </tbody>
                            </table>
                        
                        </div>
        
                        
                </div>
      
             </div>
   
            </div>

        </div>
    )
}

export default withRouter(Moderator)
