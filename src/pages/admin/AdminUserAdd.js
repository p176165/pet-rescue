import  { useState,useEffect } from 'react';
import Axios from 'axios';
import { useHistory,Link, Redirect, withRouter } from 'react-router-dom';

const Logout = () => {
   
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    localStorage.removeItem("role");
    localStorage.removeItem("email");
    
    <Redirect to={{ pathname: "/" }}/>
 };


const AdminUserAdd = () => {

    const history = useHistory();
    useEffect(() => {
        if(localStorage.getItem("role") != "admin")
        {
            history.push("/login")
        }
        
   
    }, [])

    

    const [usernamereg,setusernanmereg] = useState('')
    const [passwordreg,setpasswordreg] = useState('')
    const [rolereg, setrolereg] = useState('')
    const [statusreg,setstatusreg] = useState('')

    const register = () => {

        if(rolereg ==="admin" && localStorage.getItem("token") === null || rolereg ==="moderator" && localStorage.getItem("token") === null ){
            setstatusreg("You can't Register as " + rolereg);
            return;
        }

        Axios.post('http://localhost:3002/auth/signup',{ 
            email:usernamereg, 
            password:passwordreg, 
            role: rolereg, }).then ( (response) => {
                setstatusreg(response.data.data)
               console.log(setstatusreg(response.data.data)); 
            })
    };


    return (
        <div>
            
            <div id="blog-home" class="page">
                <div class="container">
                    <div class="row">

                        <div class="blog-sidebar bg-light-custom  h-50 border-irregular1 col-lg-3 mt-5">

                            <div class="card">
                                <h5 class="card-header">Admin Menu</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <a href="/admin" class="list-group-item list-group-item-action">Users List</a>
                                    <a href="/AdminUserAdd" class="list-group-item list-group-item-action">Add User</a>
                                    <a href="/AdminPetList" class="list-group-item list-group-item-action">Pet List</a>
                                    <a href="/AdminPetAdd" class="list-group-item list-group-item-action">Add Pet</a>
                                    </div>
                                </div>
                                <h5 class="card-header">Settings</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <Link to={"/home"} onClick={Logout} class="list-group-item list-group-item-action">Logout</Link>
                                    </div>
                                </div>
                            </div>

                        </div>      
         
                        <div class="col-lg-9">
                            <h3 class="elements-subheader">Register</h3>
                            <h4 class="bg-secondary text-white">{statusreg}</h4>
                            <div id="contact_form">
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <label>Email Adress <span class="required">*</span></label>
                                            <input type="email" name="email" class="form-control input-field" required=""  onChange={ (e) =>{ setusernanmereg(e.target.value) } } />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Password</label>
                                            <input type="password" name="subject" class="form-control input-field"  onChange={ (e) =>{ setpasswordreg(e.target.value) } } />
                                        </div>
                                        <div class="col-md-12">
                                            <label>Role<span class="required">*</span>:User</label>
                                            <input type="text" name="role" class="form-control input-field" required=""  onChange={ (e) =>{ setrolereg(e.target.value) } } />
                                        </div>
                                    </div>
                                    <button type="submit" id="submit_btn" value="Submit" class="btn btn-primary" onClick={register}>Register</button>
                                </div>
                        
                        </div>
        
                        
                </div>
      
             </div>
   
            </div>
            
        </div>
        </div>

    )
}

export default withRouter(AdminUserAdd)

