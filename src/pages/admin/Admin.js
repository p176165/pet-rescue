import React ,{useEffect,useState} from 'react';
import '../../App.css';
import { withRouter,Link,Redirect,useHistory } from 'react-router-dom';
import axios from 'axios';

const Logout = () => {
   
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    localStorage.removeItem("role");
    localStorage.removeItem("email");
    
    <Redirect to={{ pathname: "/" }}/>
 };

function fn(text){
    return text.slice(0, 8) + (text.length > 8 ? "..." : "");
}
 
function deleteUser(id){
    axios.get(`http://localhost:3002/auth/deleteuser/${id}`,{
            headers: {
               "x-access-token" : localStorage.getItem("token")
            },
        }).then((response)=>{
            
            alert("User deleted Successfully!");
            window.location.reload(false);
           
        });
}

const Admin = () => {

    const history = useHistory();

    const [userlist, setuserlist] = useState([])
    useEffect(() => {
        if(localStorage.getItem("role") != "admin")
        {
            history.push("/login")
        }
        axios.get('http://localhost:3002/auth/users',{
            headers: {
               "x-access-token" : localStorage.getItem("token")
            },
        }).then((response)=>{
            if(response.data){
                setuserlist(response.data);
            }
           
        });
   
    }, [])

    



    return (
        <div>
            
            <div id="blog-home" class="page">
                <div class="container">
                    <div class="row">

                        <div class="blog-sidebar bg-light-custom  h-50 border-irregular1 col-lg-3 mt-5">

                            <div class="card">
                                <h5 class="card-header">Admin Menu</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <a href="/admin" class="list-group-item list-group-item-action">Users List</a>
                                    <a href="/AdminUserAdd" class="list-group-item list-group-item-action">Add User</a>
                                    <a href="/AdminPetList" class="list-group-item list-group-item-action">Pet List</a>
                                    <a href="/AdminPetAdd" class="list-group-item list-group-item-action">Add New Pet</a>
                                    </div>
                                </div>
                                <h5 class="card-header">Settings</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <Link to={"/home"} onClick={Logout} class="list-group-item list-group-item-action">Logout</Link>
                                    </div>
                                </div>
                            </div>

                        </div>      
         
                        <div class="col-lg-9">
                            
                        <h3 class="elements-subheader">User List</h3>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Email</th>
                                        <th>Password</th>
                                        <th>Role</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                {userlist.map((user) => {
                                    return  (
                                        <tr key={user.id}>
                                           <td>{user.id}</td>
                                           <td>{user.email}</td>
                                           <td>{fn(user.password)}</td>
                                           <td>{user.role}</td>
                                           
                                           <td>
                                           <button type="button" class="btn btn-danger "  onClick={() => deleteUser(user.id)}>Delete</button>
                                           </td>
                                        </tr>
                                     )
                                })}
                                

                                </tbody>
                            </table>
                        
                        </div>
        
                        
                </div>
      
             </div>
   
            </div>
        </div>
        
    )
}

export default withRouter(Admin)