import  { useState,useEffect } from 'react';
import { useHistory,Link, Redirect, withRouter,useParams } from 'react-router-dom';
import axios from 'axios';

const Logout = () => {
   
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    localStorage.removeItem("role");
    localStorage.removeItem("email");
    
    <Redirect to={{ pathname: "/" }}/>
 };
 
const AdminUpDatePet = () => {

    const [updateName,setupdateName] = useState("John")
    const { id } = useParams();
    const history = useHistory();
    useEffect( async () => {
        if(localStorage.getItem("role") != "admin")
        {
            history.push("/login")
        }
       await axios.get(`http://localhost:3002/pet/updatepet/${id}`)
        .then(res => {
            setName(res.data[0].name);
            setGender(res.data[0].gender);
            setSize(res.data[0].size)
            setNeutered(res.data[0].neutered);
            setVaccinated(res.data[0].vaccinated);
            setAge(res.data[0].age);
            setBread(res.data[0].bread);
            setAbout(res.data[0].about);
            setAddedBy(res.data[0].addedBy);
            setLocation(res.data[0].location);
            setColor(res.data[0].color);
            
        })
        .catch(err =>{
            alert("Failed to Load Pet Detail Try Again!")
            history.push("/AdminPetList")
        })
        
   
    }, [])

    const [name,setName] = useState("")
    const [gender,setGender] = useState("")
    const [size,setSize] = useState("")
    const [neutered,setNeutered] = useState("")
    const [vaccinated,setVaccinated] = useState("")
    const [age,setAge] = useState("")
    const [bread,setBread] = useState("")
    const [description,setAbout] = useState("")
    const [addedBy,setAddedBy] = useState("")
    const [location,setLocation] = useState("")
    const [color,setColor] = useState("")
    const [file,setFile] = useState("")
    const [petStatus,setpetStatus] = useState("")

    const send = async (event) => {
        
        event.preventDefault();
       const  user = localStorage.getItem("email");
        
        const data = new FormData()
        data.append("name",name);
        data.append("gender",gender);
        data.append("size",size);
        data.append("neutered",neutered);
        data.append("vaccinated",vaccinated);
        data.append("age",age);
        data.append("bread",bread);
        data.append("addedby",user);
        data.append("about",description);
        data.append("location",location);
        data.append("color",color);
        data.append("file",file);
        
        
      await  axios.post(`http://localhost:3002/pet//updatepet/${id}`,data)
        .then(res => {
            history.push("/AdminPetList")
        })
        .catch(err =>{
            alert("Failed to Update!")
            history.push("/AdminPetList")
        })
       
        
    };   



    return (
        <div>
            
            <div id="blog-home" class="page">
                <div class="container">
                    <div class="row">

                        <div class="blog-sidebar bg-light-custom  h-50 border-irregular1 col-lg-3 mt-5">

                            <div class="card">
                                <h5 class="card-header">Admin Menu</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <a href="/admin" class="list-group-item list-group-item-action">Users List</a>
                                    <a href="/AdminUserAdd" class="list-group-item list-group-item-action">Add User</a>
                                    <a href="/AdminPetList" class="list-group-item list-group-item-action">Pet List</a>
                                    <a href="/AdminPetAdd" class="list-group-item list-group-item-action">Add New Pet</a>
                                    </div>
                                </div>
                                <h5 class="card-header">Settings</h5>
                                <div class="card-body">
                                    <div class="list-group">
                                    <Link to={"/home"} onClick={Logout} class="list-group-item list-group-item-action">Logout</Link>
                                    </div>
                                </div>
                            </div>

                        </div>      
         
                        <div class="col-lg-9">

                            <h3 class="elements-subheader">Update Pet</h3>
                            <h4 class="bg-secondary text-white"></h4>
                            <form>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Name<span class="required">*</span></label>
                                            <input type="text" name="name" class="form-control input-field" required="" placeholder="Fluffy" onChange={ (e) =>{ setName(e.target.value) } }  value={name} />

                                        </div>
                                        <div class="col-md-4">
                                            <label>Gender<span class="required">* : Male,Female</span></label>
                                            <input type="text" name="gender" class="form-control input-field" required="" placeholder="Female" onChange={ (e) =>{ setGender(e.target.value) } } value={gender} />

                                        </div>
                                        <div class="col-md-4">
                                            <label>Size<span class="required">* : Small,Medium,Big</span></label>
                                            <input type="text" name="size" class="form-control input-field" required="" placeholder="Medium" onChange={ (e) =>{ setSize(e.target.value) } }  value={size} />

                                        </div>
                                        
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                        <label>Neutered<span class="required">*: Yes,No</span></label>
                                            <input type="text" name="neutered" class="form-control input-field"  required="" placeholder="Yes" onChange={ (e) =>{ setNeutered(e.target.value) } }  value={neutered} />

                                        </div>
                                        <div class="col-md-3">
                                            <label>Vaccinated<span class="required">*: Yes,No</span></label>
                                            <input type="text" name="vaccinated" class="form-control input-field"  required="" placeholder="Yes" onChange={ (e) =>{ setVaccinated(e.target.value) } }  value={vaccinated} />
                                            
                                        </div>
                                        <div class="col-md-2">
                                            <label>Age<span class="required">*</span></label>
                                            <input type="number" name="age" class="form-control input-field"  required="" placeholder="2" onChange={ (e) =>{ setAge(e.target.value) } }  value={age} />
                                            
                                        </div>
                                        <div class="col-md-4">
                                            <label>Breed<span class="required">*</span></label>
                                            <input type="text" name="bread" class="form-control input-field" placeholder="Poodle" onChange={ (e) =>{ setBread(e.target.value) } } value={bread} />
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <label>Location<span class="required">*</span></label>
                                            <input type="text" name="location" class="form-control input-field"  onChange={ (e) =>{ setLocation(e.target.value) } }  value={location} />
                                        </div>
                                        <div class="col-md-4">
                                            <label>Color<span class="required">*</span></label>
                                            <input type="text" name="color" class="form-control input-field"  onChange={ (e) =>{ setColor(e.target.value) } } value={color} />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Image</label>
                                            <input type="file" name="file" class="form-control input-field" accept="image/*" onChange={ (e) =>{ setFile(e.target.files[0]) } } required=""   />
                                        </div>
                                        <div class="col-md-12">
                                            <label>About<span class="required">*</span></label>
                                            <textarea name="about"  class="textarea-field form-control" rows="3" required=""  onChange={ (e) =>{ setAbout(e.target.value) } }  value={description}  />
                                        </div>
                                    </div>
                                    <button type="submit" id="submit_btn" value="Submit" class="btn btn-primary" onClick={send} >Add Now</button>
                                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withRouter( AdminUpDatePet ) 
