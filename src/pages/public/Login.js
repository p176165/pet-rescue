import  { useState } from 'react';
import Axios from 'axios';
import { useHistory } from 'react-router-dom';



const Login = () => {

    let history = useHistory();

    const [usernamereg,setusernanmereg] = useState('')
    const [passwordreg,setpasswordreg] = useState('')
    const [rolereg, setrolereg] = useState('')
    const [statusreg,setstatusreg] = useState('')

    const [username,setusernanme] = useState('')
    const [password,setpassword] = useState('')
    const [role, setrole] = useState('')
    const [statuslogin,setloginstatus] = useState('')

    

    const register = () => {

        if(rolereg ==="admin" && localStorage.getItem("token") === null || rolereg ==="moderator" && localStorage.getItem("token") === null ){
            setstatusreg("You can't Register as " + rolereg);
            return;
        }

        Axios.post('http://localhost:3002/auth/signup',{ 
            email:usernamereg, 
            password:passwordreg, 
            role: rolereg, }).then ( (response) => {
                setstatusreg(response.data.data)
               console.log(setstatusreg(response.data.data)); 
            })
    };


    const login = () => {
        console.log({ 
            email:username, 
            password:password, 
            role: role, });
        Axios.post('http://localhost:3002/auth/signin',{ 
            email:username, 
            password:password, 
            role: role, }).then ( (response) => {
                //console.log(response);
                if(response.data.auth === true){
                    localStorage.setItem("token",response.data.token);
                    localStorage.setItem("id",response.data.result[0].id);
                    localStorage.setItem("role",response.data.result[0].role);
                    localStorage.setItem("email",response.data.result[0].email);
                    if(response.data.result[0].role === 'user'){
                        
                        history.push('/contact');
                    }
                    if(response.data.result[0].role === 'admin'){
                        
                        history.push('/admin');
                    }
                    if(response.data.result[0].role === 'moderator'){
                        
                        history.push('/moderator');
                    }
                    if(response.data.result[0].role === 'user'){
                        
                        history.push('/user');
                    }
                }
                else{
                    setloginstatus(response.data.msg)

                }
                
            })
    };




    return (
        <div>
            <div class="jumbotron jumbotron-fluid overlay">
                <div class="jumbo-heading">

                    <div class="section-heading" data-aos="zoom-in">
                        <h1>Login/Register</h1>
                    </div>

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.phtml">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Login/Register</li>
                        </ol>
                    </nav>
                </div>

            </div>

            <div class="page pb-0">
                <div class="container">
                    <div class="row">

                        <div class="contact-info col-lg-5">
                            <h4 class="bg-secondary text-white">{statusreg}</h4>
                            <h4>Register!</h4>

                            <div id="contact_form">
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <label>Email Adress <span class="required">*</span></label>
                                            <input type="email" name="email" class="form-control input-field" required=""  onChange={ (e) =>{ setusernanmereg(e.target.value) } } />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Password</label>
                                            <input type="password" name="subject" class="form-control input-field"  onChange={ (e) =>{ setpasswordreg(e.target.value) } } />
                                        </div>
                                        <div class="col-md-12">
                                            <label>Role<span class="required">*</span>:User</label>
                                            <input type="text" name="role" class="form-control input-field" required=""  onChange={ (e) =>{ setrolereg(e.target.value) } } />
                                        </div>
                                    </div>
                                    <button type="submit" id="submit_btn" value="Submit" class="btn btn-primary" onClick={register}>Register</button>
                                </div>

                                <div id="contact_results"></div>
                            </div>

                        </div>


                        <div class="contact-info col-lg-5 offset-lg-1 h-50 res-margin">
                        <p class="bg-secondary text-white">{statuslogin}</p>
                        <h4>Login!</h4>

                        <div id="contact_form">
                            <div class="form-group">
                                <div class="row">

                                    <div class="col-md-12">
                                        <label>Email Adress <span class="required">*</span></label>
                                        <input type="email" name="email" class="form-control input-field" required=""  onChange={ (e) =>{ setusernanme(e.target.value) } } />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Password</label>
                                        <input type="password" name="subject" class="form-control input-field"  onChange={ (e) =>{ setpassword(e.target.value) } } />
                                    </div>
                                    <div class="col-md-12">
                                        <label>Role<span class="required">*</span>:User,Moderator,Admin</label>
                                        <input type="text" name="role" class="form-control input-field" required=""  onChange={ (e) =>{ setrole(e.target.value) } } />
                                    </div>
                                </div>
                                <button type="submit" id="submit_btn" value="Submit" class="btn btn-primary" onClick={login}>Login</button>
                            </div>

                            <div id="contact_results"></div>
                            

                            </div>

                        </div>


                    </div>

                </div>

            </div>
        </div>

    )
}

export default Login
