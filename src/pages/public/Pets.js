import React ,{useEffect,useState} from 'react';
import '../../App.css';
//import { withRouter,Link,Redirect,useHistory } from 'react-router-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';


const Pets = () => {

    const [searchTerm,setsearchTerm] = useState("")
    //const history = useHistory();
    const image = (file) => {
        return "uploads/" + file;
    }

    const [petlist, setpetlist] = useState([])
    useEffect(() => {
        
        axios.get('http://localhost:3002/pet/allpets').then((response)=>{
            if(response.data){
                setpetlist(response.data.reverse());
            }
           
        });
   
    }, [])


    return (
        
        <div>

            <div class="jumbotron jumbotron-fluid overlay">
            <div class="jumbo-heading">
                
                <div class="section-heading" data-aos="zoom-in">
                    <h1>Pets</h1>
                </div>
                
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.phtml">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Pets</li>
                    </ol>
                </nav>
            </div>
           
            </div>
            <section id="adopt" class="paws-house-bg1 bg-light">
                <div class="container ">
                    
                    <div class="col-lg-10 offset-lg-1 text-center">
                        <h3>Find a new furry Friend</h3>
                        <p>Maecenas at arcu risus. Donec commodo sodales ex, scelerisque laoreet nibh hendrerit id. In aliquet magna nec lobortis maximus. Etiam rhoncus leo a dolor placerat, nec elementum ipsum convall.</p>
                    </div>
                    <div class="row">
                        <div class="col-lg-8"></div>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" placeholder="Search by Age,Location,Color and Name" onChange={(event) =>{
                                setsearchTerm(event.target.value);
                            }} />
                            
                        </div>
                    </div>
                    <div class="row mt-5">
                        
                        

                            {petlist.filter((pet) => {
                                if(searchTerm == ""){
                                    return pet;
                                }else if(pet.name.toLowerCase().includes(searchTerm.toLowerCase()) || pet.age <= searchTerm || pet.location.toLowerCase().includes(searchTerm.toLowerCase()) || pet.color.toLowerCase().includes(searchTerm.toLowerCase()) ){
                                    return pet;
                                }
                            }).map((pet) => {
                                    return  (
                                        <div class="adopt-card col-md-6 col-xl-3 res-margin">
                                        <div class="card bg-light-custom">
                                        <div class="thumbnail text-center">
                                            
                                            <img src={image(pet.file)} class="border-irregular1 img-fluid" alt="" />
                                            
                                            <div class="caption-adoption">
                                                <h6 class="adoption-header">{pet.name}</h6>
                                            
                                                <ul class="list-unstyled">
                                                    <li><strong>Gender:</strong>{pet.gender}</li>
                                                    <li><strong>Neutered: </strong>{pet.neutered}</li>
                                                    <li><strong>Age:</strong> {pet.age} Months</li>
                                                    <li><strong>Color: </strong>{pet.color}</li>
                                                    <li><strong>Location:</strong> {pet.location}</li>
                                                </ul>
                                                                                
                                                <div class="text-center">
                                                    <Link to={`SinglePet/${pet.id}`} class="btn btn-primary">More Info</Link>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                            
                                       </div>
                                     )
                                })}
                                
                            
                            
                            
                        
                        
                    </div>
                      
                </div>
                
            </section>


        </div>    

    )
}

export default Pets
