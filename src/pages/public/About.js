import React from 'react'

const About = () => {
    return (
        <div>
            <div class="jumbotron jumbotron-fluid overlay">
   <div class="jumbo-heading">
     
      <div class="section-heading" data-aos="zoom-in">
         <h1>About Us</h1>
      </div>
     
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.phtml">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">About us</li>
         </ol>
      </nav>
   </div>
  
</div>

<div class="page">
   <div class="container block-padding pt-0">
      <div class="row">
         <div class="col-lg-6">
            <h3>Caring for your pets</h3>
            <p>Aliquam erat volutpat In id fermentum augue, ut pellentesque leo. Maecenas at arcu risus. Donec commodo sodales ex, scelerisque laoreet nibh hendrerit id. In aliquet magna nec lobortis maximus. Etiam rhoncus leo a dolor placerat, nec elementum ipsum convall.</p>
            <p>Etiam rhoncus leo a dolor placerat, nec elementum ipsum convall Maecenas at arcu risus scelerisque laoree.</p>
            <ul class="custom pl-0">
               <li>Orci eget, viverra elit liquam erat volut pat phas ellus ac</li>
               <li>Ipuset phas ellus ac sodales Lorem ipsum dolor Phas ell</li>
               <li>Aliquam erat volut pat phas ellu</li>
            </ul>
            
         </div>
		 
         <div class="col-lg-6">
            <img src="img/about2.jpg" alt="" class="img-fluid border-irregular1"   data-aos="zoom-in" />
         </div>
      </div>
	  
   </div>
   
   <div class="bg-light-custom ">
      <div class="container">
         <div id="counter" class="row">
           
            <div class="col-xl-3 col-md-6">
               <div class="counter">
                  <i class="counter-icon fa fa-users"></i>
				
				  <div class="counter-value" data-count="1500">0</div>
                  <h3 class="title">Happy Clients</h3>
               </div>
              
            </div>
            
            <div class="col-xl-3 col-md-6">
               <div class="counter">
                  <i class="counter-icon flaticon-dog-in-front-of-a-man"></i>
				 
				  <div class="counter-value" data-count="14">0</div>
                  <h3 class="title">Professionals</h3>
               </div>
              
            </div>
            
            
            <div class="col-xl-3 col-md-6">
               <div class="counter">
                  <i class="counter-icon flaticon-dog-2"></i>
				
				  <div class="counter-value" data-count="900">0</div>
                  <h3 class="title">Total Pets</h3>
               </div>
              
            </div>
            
            <div class="col-xl-3 col-md-6">
               <div class="counter">
                  <i class="counter-icon flaticon-prize-badge-with-paw-print"></i>
				
				  <div class="counter-value" data-count="12">0</div>
                  <h3 class="title">Prizes</h3>
               </div>
             
            </div>
           
         </div>
		 
      </div>
	  
   </div>
    
	 
        
   </div>
        </div>
    )
}

export default About
