import React, {useState,useEffect} from 'react'
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import Favorite from '../user/Favorite';

function fn(text){
    return text.slice(0, 35) + (text.length > 35 ? "..." : "");
}






const SinglePet = () => {

    const { id } = useParams()
    
    const image = (file) => {
        return "/uploads/" + file;
    }

    const [petDetail, setpetDetail] = useState([])
    const [petid,setPetid] = useState("")
    const [petname,setPetname] = useState("")
    useEffect(() => {
        
        axios.get(`http://localhost:3002/pet/singlepet/${id}`).then((response)=>{
            if(response.data){
                setpetDetail(response.data);
                
            }
           
        });
   
    }, [])

    function Rolebasedbtnoptions(roomid) {
        if(localStorage.getItem("role") === "user")
        {
           return(
                <div>
                    <Favorite petid={petDetail[0].id} petname = {petDetail[0].name}  />
                    <Link to={`/messages/${roomid}`} class="btn btn-primary " data-aos="zoom-in" >Message Now!</Link>
                </div>
           )
        }
        if(localStorage.getItem("role") === null)
        {
           return(
              <div>
                  <a href="/login" class="btn btn-primary " data-aos="zoom-in" >Register Now For More Detail!</a>
                  
              </div>
           )
        }
        
     }

    return (
        <div>
            <div class="jumbotron jumbotron-fluid overlay">
                <div class="jumbo-heading">
                    
                    <div class="section-heading" data-aos="zoom-in">
                        <h1>Pet Info</h1>
                    </div>
                    
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.phtml">Home</a></li>
                            <li class="breadcrumb-item"><a href="adoption-2.html">pets</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Pet Info</li>
                        </ol>
                    </nav>
                </div>
                
            </div>
            
            {petDetail.map((pet) => {
                
                return(
            <div class="page">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row bg-light-custom border-irregular1 block-padding">
                           
                            <div class="col-lg-4 offset-lg-2">
                               
                            <div class="col-md-12">
                                        
                                        
                                        <img src={image(pet.file)} class="border-irregular1 img-fluid hover-opacity" alt="" />
                                        
                                    </div>
                               	
                            </div>
                           
                            <div class="col-lg-4 res-margin mt-5 text-xs-center">
                                <h4><strong>Pet name:</strong>{pet.name}</h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <ul class="list-unstyled pet-adopt-info">
                                            <li class="h7">Gender: <span>{pet.gender}</span></li>
                                            <li class="h7">Age: <span>{pet.age} years</span></li>
                                            <li class="h7">Breed: <span>{pet.bread}</span></li>
                                            <li class="h7">Color: <span>{pet.color}</span></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6">
                                        <ul class="list-unstyled pet-adopt-info">
                                            <li class="h7">Neutered: <span>{pet.neutered}</span></li>
                                            <li class="h7">Vaccinated: <span>{pet.vaccinated}</span></li>
                                            <li class="h7">Size: <span>{pet.size}</span></li>
                                            <li class="h7">Location: <span>{pet.location}</span></li>
                                        </ul>
                                    </div>
                               
                                </div>
                                
                                <p class="font-weight-bold">{fn(pet.about)}</p>
                               
                                {Rolebasedbtnoptions(pet.user)}            
                            </div>
                            
                </div>
                           			   
                            <div class="col-md-12 mt-5">
                            <h3>About {pet.name}</h3>
                               
                                <ul class="custom list-inline font-weight-bold">
                                    <li class="list-inline-item">Friendly to other dogs</li>
                                    <li class="list-inline-item">Good for Apartments</li>
                                    <li class="list-inline-item">Friendly with Kids</li>
                                </ul>
                                <p>{pet.about}</p>
                                
                                
                                
                            </div>
                           
                        
                        </div>
                       
                    </div>
                   
                </div>
               
                </div>
                )
            })}

        </div>
    )
}

export default SinglePet
