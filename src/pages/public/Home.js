import React ,{useEffect,useState} from 'react';
import '../../App.css';
import { Link } from 'react-router-dom';
import axios from 'axios';




const Home = () => {

    //const history = useHistory();
    const image = (file) => {
        return "uploads/" + file;
    }

    const [petlist, setpetlist] = useState([])
    useEffect(() => {
        
        axios.get('http://localhost:3002/pet/petsHome').then((response)=>{
            if(response.data){
                setpetlist(response.data);
            }
           
        });
   
    }, [])
    
    return (
        <div>
            
            <div id="video-header" class="embed-responsive embed-responsive-16by9">
                <div class="video-overlay"></div>
            
                <video id="bgvid" playsinline autoplay muted loop>
                
                    <source src="videos/video.webm" type="video/webm" />
                    <source src="videos/video.mp4" type="video/mp4" />
                </video>

                <div class="video-text">
                    	
                
                    <div class="navbar-brand">
                        <i class="flaticon-dog-20"><span>Pet Rescue!</span></i>
                    </div>
                </div>
                
           </div>

        
           
            <section id="about" class="dog-bg2">
            <div class="container">
                <div class="section-heading text-center">
                    <h2>About Us</h2>
                </div>
                
            </div>
            
                <div class="container block-padding pt-0">
                <div class="row">
                    <div class="col-lg-6">
                        <h3>Caring for your pets</h3>
                        <p>Aliquam erat volutpat In id fermentum augue, ut pellentesque leo. Maecenas at arcu risus. Donec commodo sodales ex, scelerisque laoreet nibh hendrerit id. In aliquet magna nec lobortis maximus. Etiam rhoncus leo a dolor placerat, nec elementum ipsum convall.</p>
                        <p>Etiam rhoncus leo a dolor placerat, nec elementum ipsum convall Maecenas at arcu risus scelerisque laoree.</p>
                        <ul class="custom pl-0">
                        <li>Orci eget, viverra elit liquam erat volut pat phas ellus ac</li>
                        <li>Ipuset phas ellus ac sodales Lorem ipsum dolor Phas ell</li>
                        <li>Aliquam erat volut pat phas ellu</li>
                        </ul>
                        
                    </div>
                    
                    <div class="col-lg-6">
                        <img src="img/gallery/gallery10.jpg" alt="" class="img-fluid border-irregular1"   data-aos="zoom-in" />
                    </div>
                </div>
               
            </div>
            </section>


            <section id="adopt" class="paws-house-bg1 bg-light">
                <div class="container ">
                    <div class="section-heading text-center">
                        <h2>Adopt a Pet</h2>
                    </div>
                    
                    <div class="col-lg-10 offset-lg-1 text-center">
                        <h3>Find a new furry Friend</h3>
                        <p>Maecenas at arcu risus. Donec commodo sodales ex, scelerisque laoreet nibh hendrerit id. In aliquet magna nec lobortis maximus. Etiam rhoncus leo a dolor placerat, nec elementum ipsum convall.</p>
                    </div>
                    
                    <div class="row mt-5">
                        
                        

                            {petlist.map((pet) => {
                                    return  (
                                        <div class="adopt-card col-md-6 col-xl-3 res-margin">
                                        <div class="card bg-light-custom">
                                        <div class="thumbnail text-center">
                                            
                                            <img src={image(pet.file)} class="border-irregular1 img-fluid" alt="" />
                                            
                                            <div class="caption-adoption">
                                                <h6 class="adoption-header">{pet.name}</h6>
                                            
                                                <ul class="list-unstyled">
                                                    <li><strong>Gender:</strong>{pet.gender}</li>
                                                    <li><strong>Neutered: </strong>{pet.neutered}</li>
                                                    <li><strong>Age:</strong> {pet.age} years</li>
                                                </ul>
                                                                                
                                                <div class="text-center">
                                                    <Link to={`SinglePet/${pet.id}`} class="btn btn-primary">More Info</Link>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                            
                                       </div>
                                     )
                                })}
                                
                            
                            
                            
                        
                        
                    </div>
                       <div class="text-center mt-5">
                        <a href="/pets" class="btn btn-secondary btn-lg">See more pets</a>
                    </div>
                </div>
                
            </section>


        </div>
    )
}

export default Home
