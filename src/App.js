
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import {useState,useEffect} from 'react'

import './App.css';

import Contact from './pages//public/Contact';
import Footer from './components/Footer';
import Header from './components/Header';
import Home from './pages//public/Home';
import Login from './pages//public/Login';
import About from './pages/public/About'
import Admin from './pages/admin/Admin';
import AdminUserAdd from './pages/admin/AdminUserAdd';
import AdminPetAdd from './pages/admin/AdminPetAdd';
import ProtectedRoute from './ProtectedRoute';
import User from './pages/user/User';
import Moderator from './pages/moderator/Moderator'
import AdminPetList from './pages/admin/AdminPetList';
import ModeratorPetAdd from './pages/moderator/ModeratorPetAdd';
import Pets from './pages/public/Pets';
import Messages from './pages/user/Messages';
import AdminUpDatePet from './pages/admin/AdminUpDatePet';
import ModUpDatePet from './pages/moderator/ModUpDatePet';
import SinglePet from './pages/public/SinglePet';
import ModeratorMessage from './pages/moderator/ModeratorMessage'




function App() {

  const [roleIs, setroleIs] = useState('');
  const [isAuth, setisAuth] = useState(true);

  useEffect(() => {
    if(localStorage.getItem("token"))
    {
      console.log(localStorage.getItem("role"));
      setroleIs(localStorage.getItem("role"));
      setisAuth(true);
    }
    else{
      setroleIs('');
      
    }
    
  }, []);

  return (
    <div className="App">
      <Router>
        
        <Header isLoggedIn={roleIs} />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/pets" component={Pets} />
          <Route exact path="/singlepet/:id" component={SinglePet} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/about" component={About} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/home" component={Home} />
          <ProtectedRoute  path="/admin" component={Admin} isAuth={ isAuth }  />
          <ProtectedRoute  path="/AdminUserAdd" component={AdminUserAdd} isAuth={ isAuth } />
          <ProtectedRoute  path="/user" component={User} isAuth={ isAuth } />
          <ProtectedRoute  path="/moderator" component={Moderator} isAuth={ isAuth } />
          <ProtectedRoute  path="/AdminPetAdd" component={AdminPetAdd} isAuth={ isAuth }/>
          <ProtectedRoute  path="/AdminPetList" component={AdminPetList} isAuth={ isAuth }/>
          <ProtectedRoute  path="/ModeratorPetAdd" component={ModeratorPetAdd} isAuth={ isAuth }/>
          <ProtectedRoute  path="/AdminUpDatePet/:id" component={AdminUpDatePet} isAuth={ isAuth }/>
          <ProtectedRoute  path="/ModUpDatePet/:id" component={ModUpDatePet} isAuth={ isAuth }/>
          <ProtectedRoute  path="/messages/:roomid" component={Messages} isAuth={ isAuth } />
          <ProtectedRoute  path="/ModeratorMessage/" component={ModeratorMessage} isAuth={ isAuth } />
          
          
        </Switch>
        
        <Footer />
      </Router>
    </div>
  );
}

export default App;
