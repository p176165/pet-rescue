
var express = require('express');
var router = express.Router();
var petModel = require('../models/pet-model');
var bcrypt = require('bcrypt');
var db = require("../db");
const { response } = require('express');
var jwt = require('jsonwebtoken');
var multer = require('multer');
const fs = require("fs");
const { promisify } = require("util");
const { Stream } = require('stream');
const pipeline = promisify(require("stream").pipeline);
const path = require("path");



const DIR = '../public/uploads';

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR);
    },
    filename: (req, file, cb) => {
        const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, Math.floor(Math.random() *1000) + '-' + fileName)
    }
});

var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }
});


const verifyjwt = (req,res,next) => {
     const token = req.headers['x-access-token'];
     if(!token){
         res.send({msg : "we need tokken"})
     }else{
         jwt.verify(token,"jwtsecret",(err,decoded) => {
             if(err){
                 res.json({auth : false, msg: "You failed to athenticate"})

             }else{
                req.id = decoded.id;
                next()
             }
         })
     }
}



router.post('/addPet',upload.single('file'), function(req, res, next) {

    const petDetail = {
        name: req.body.name,
        gender: req.body.gender,
        size: req.body.size,
        neutered: req.body.neutered,
        vaccinated: req.body.vaccinated,
        age: req.body.age,
        bread: req.body.bread,
        location: req.body.location,
        color: req.body.color,
        about: req.body.about,
        addedby: req.body.email,
        file: req.file.filename
      }
      
      petModel.petAdd(petDetail, function(err, result) {
        res.json({ data: "Pet Detail Added Successfully!", error: err })
    });
     
});


router.get('/pets',verifyjwt,(req,res) => {

    db.query("SELECT * FROM pets ",(err,result) =>{
        if(err){
            res.send({err:err});
        }
        else{
            
            res.send(result);
        }
    })
})


router.get('/Modpets/:user',verifyjwt,(req,res) => {
    
    db.query(`SELECT * FROM pets WHERE user='${req.params.user}'`,(err,result) =>{
        if(err){
            res.send({err:err});
        }
        else{
            
            res.send(result);
        }
    })
})



router.get('/updatepet/:id',(req,res) => {

    
    db.query(`SELECT * FROM pets where id=${req.params.id} `,(err,result) =>{
        if(err){
            res.send({err:err});
        }
        else{
            
            res.send(result);
        }
    })
})

router.post('/updatepet/:id',upload.single('file'),(req,res,next) => {

    const petDetail = {
        id:req.params.id,
        name: req.body.name,
        gender: req.body.gender,
        size: req.body.size,
        neutered: req.body.neutered,
        vaccinated: req.body.vaccinated,
        age: req.body.age,
        bread: req.body.bread,
        location: req.body.location,
        color: req.body.color,
        about: req.body.about,
        addedby: req.body.addedby,
        file: req.file.filename
      }
      petModel.petupdate(petDetail, function(err, result) {
        res.status(200).json({ data: "Pet Detail Updated Successfully!", error: err })
    });
    
})




router.get('/singlepet/:id',(req,res) => {

    
    db.query(`SELECT * FROM pets where id=${req.params.id} `,(err,result) =>{
        if(err){
            res.send({err:err});
        }
        else{
            
            res.send(result);
        }
    })
})




router.get('/deletepet/:id',verifyjwt,(req,res) => {

    
    db.query(`Delete FROM pets where id=${req.params.id} `,(err,result) =>{
        if(err){
            res.send({err:err});
        }
        else{
            console.log(result)
            res.send(result);
        }
    })
})








router.get('/petsHome',(req,res) => {

    db.query("SELECT * FROM pets LIMIT 4 ",(err,result) =>{
        if(err){
            res.send({err:err});
        }
        else{
            
            res.send(result);
        }
    })
})

router.get('/allpets',(req,res) => {

    db.query("SELECT * FROM pets  ",(err,result) =>{
        if(err){
            res.send({err:err});
        }
        else{
            
            res.send(result);
        }
    })
})





module.exports = router;
