
var express = require('express');
var router = express.Router();
var authModel = require('../models/auth-model');
var bcrypt = require('bcrypt');
var db = require("../db");
const { response } = require('express');
var jwt = require('jsonwebtoken');


const verifyjwt = (req,res,next) => {
     const token = req.headers['x-access-token'];
     if(!token){
         res.send({msg : "we need tokken"})
     }else{
         jwt.verify(token,"jwtsecret",(err,decoded) => {
             if(err){
                 res.json({auth : false, msg: "You failed to athenticate"})

             }else{
                req.id = decoded.id;
                next()
             }
         })
     }
}

router.post('/signup', function(req, res) {
    const password = req.body.password;
    const saltRounds = 10;
    bcrypt.hash(password, saltRounds, function(err, hash) {
        req.body.password = hash;
        console.log(res.body);
        authModel.signup(req.body, function(err, result) {
            res.json({ data: "User Register Successfully!", error: err })
        });
    });
});


router.post('/signin', function(req, res) {
    
        
        
        db.query("SELECT * FROM users WHERE email=? AND role=?",
         [req.body.email,req.body.role], 
         (err,result) =>{
             if(err){
                 res.send({err:err});
             }

             if(result.length > 0)
             {
                 bcrypt.compare(req.body.password,result[0].password,(error,response) => {
                     if(response){
                         const id = result[0].id;
                         const token = jwt.sign({id},"jwtsecret",{
                             expiresIn: 1500,
                         })
                         res.json({auth : true, token:token, result: result});
                     }else{
                        res.json({auth : false, msg:'Wrong username or password'});
                     }
                 })
             }
             else{
                res.json({auth : false, msg:"User dosn't exits or you don't have permessions to enter as "+ req.body.role});
                 
             }

             //res.send(result);
         })
    });
   // });

router.get('/users',verifyjwt,(req,res) => {

    db.query("SELECT * FROM users ",(err,result) =>{
        if(err){
            res.send({err:err});
        }
        else{
            console.log(result)
            res.send(result);
        }
    })
})


router.get('/deleteuser/:id',verifyjwt,(req,res) => {

    
    db.query(`Delete FROM users where id=${req.params.id} `,(err,result) =>{
        if(err){
            res.send({err:err});
        }
        else{
            console.log(result)
            res.send(result);
        }
    })
})



module.exports = router;
