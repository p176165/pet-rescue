
var db = require("../db");

let model = {
    petAdd: (input, cb) => {

        let data = {
            name: input.name,
            gender: input.gender,
            size: input.size,
            neutered: input.neutered,
            vaccinated: input.vaccinated,
            age: input.age,
            bread: input.bread,
            about: input.about,
            user: input.addedby,
            file: input.file,
            location:input.location,
            color:input.color
            
        };
        console.log(data)
        return db.query("INSERT INTO pets SET ?", [data], cb)
    },
    petupdate: (input, cb) => {
        
        let data = {
            id: input.id,
            name: input.name,
            gender: input.gender,
            size: input.size,
            neutered: input.neutered,
            vaccinated: input.vaccinated,
            age: input.age,
            bread: input.bread,
            about: input.about,
            user: input.addedby,
            file: input.file,
            location: input.location,
            color: input.color,
            
        };
        
        return db.query(`UPDATE pets SET name='${data.name}', gender='${data.gender}',size='${data.size}',neutered='${data.neutered}',vaccinated='${data.vaccinated}', age='${data.age}',bread='${data.bread}',about='${data.about}',user='${data.user}',file='${data.file}',color='${data.color}',location='${data.location}' WHERE id=${data.id}`,cb)
    }

    

       
}

module.exports = model;
